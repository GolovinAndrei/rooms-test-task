package by.golovin.rooms.repository;

import by.golovin.rooms.entity.Country;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CountryRepository extends CrudRepository<Country, Integer> {
    @Override
    List<Country> findAll();
    Optional<Country> findByCountryName(String countryName);

}

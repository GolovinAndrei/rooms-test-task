package by.golovin.rooms.controller;

import by.golovin.rooms.entity.Country;
import by.golovin.rooms.entity.Room;
import by.golovin.rooms.service.AccessControl;
import by.golovin.rooms.service.CountryRepositoryService;
import by.golovin.rooms.service.MyConst;
import by.golovin.rooms.service.RoomRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class RoomController {

    @Autowired
    RoomRepositoryService roomRepositoryService;

    @Autowired
    CountryRepositoryService countryRepositoryService;

    @Autowired
    AccessControl accessControl;


    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
    public String mainPage(Model model) {
        List<Room> rooms = roomRepositoryService.allRooms();
        List<Country> countries = countryRepositoryService.allCountries();
        model.addAttribute("rooms", rooms);
        model.addAttribute("countries", countries);
        model.addAttribute("room", new Room());
        return "index";
    }

    @PostMapping("/addRoom")
    public String addRoom(Model model, @ModelAttribute("room") Room room) {
        if (room.getCountry()==null || room.getName()==null){
            model.addAttribute("errorMessage", MyConst.EMPTY_FIELDS);
            return "index";
        }
        roomRepositoryService.save(room);
        return "redirect:/";
    }

    @RequestMapping(value = {"/openRoom/{id}" }, method = RequestMethod.GET)
    public String openRoom(@PathVariable int id, Model model) {
        Room curRoom = roomRepositoryService.findRoomById(id);
        if (curRoom.getCountry().getIsoCode().equalsIgnoreCase(accessControl.countryCode())) {
            if (curRoom.getEnabledLamp()) {
                model.addAttribute("lamp", "Lamp is ON!");
            } else {
                model.addAttribute("lamp", "Lamp OFF!");
            }
            model.addAttribute("roomNumber", id);
            return "openRoom";
        }
        model.addAttribute("WrongRoomMessage", MyConst.WRONG_ROOM);
        return "index";
    }

    @PostMapping("/switch")
    public String switcher (@RequestParam("lampId") int id) {
        Room curRoom = roomRepositoryService.findRoomById(id);
        if(curRoom.getEnabledLamp()){
            curRoom.setEnabledLamp(false);
        } else {
            curRoom.setEnabledLamp(true);
        }
        roomRepositoryService.save(curRoom);
        return "redirect:/";
    }


}

package by.golovin.rooms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication

@ComponentScan("by.golovin.rooms.*")
public class RoomsTestTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(RoomsTestTaskApplication.class, args);
	}

}

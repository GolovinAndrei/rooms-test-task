package by.golovin.rooms.service;

import by.golovin.rooms.entity.Country;
import by.golovin.rooms.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryRepositoryService {

    @Autowired
    CountryRepository countryRepository;

    public List<Country> allCountries() {
        return countryRepository.findAll();
    }
}

package by.golovin.rooms.service;

public class MyConst {

    public static final String IPDB_PATH = "D:/Java Learning/rooms-testTask/src/main/resources/GeoIpDB/GeoLite2-City.mmdb";

    public static final String EMPTY_FIELDS = "Error! Fields are empty!";

    public static final String WRONG_ROOM ="Access denied!";
}

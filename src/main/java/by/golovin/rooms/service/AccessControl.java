package by.golovin.rooms.service;

import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.record.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CityResponse;

@Service
public class AccessControl {

    @Autowired
    private HttpServletRequest request;


    public String findLocation (String ip)  {
    String countryCode = "";
    try {
        File dbFile = new File(MyConst.IPDB_PATH);
        DatabaseReader reader = new DatabaseReader.Builder(dbFile).build();
        InetAddress ipAddress = InetAddress.getByName(ip);
        CityResponse response = reader.city(ipAddress);
        Country country = response.getCountry();
        countryCode=country.getIsoCode();
    } catch (IOException | GeoIp2Exception e){
        e.printStackTrace();
    }
        return countryCode;
}

    public String findRemoteIP (HttpServletRequest request) {
    String remoteIP = "";
        if(request !=null){
            remoteIP = request.getHeader("X-FORWARDED-FOR");
            if (remoteIP == null || "".equals(remoteIP)) {
                remoteIP = request.getRemoteAddr();
            }
        }
    return remoteIP;
    }

    public String countryCode (){
        return findLocation(findRemoteIP(request));
    }

}

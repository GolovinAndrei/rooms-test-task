package by.golovin.rooms.service;

import by.golovin.rooms.entity.Room;
import by.golovin.rooms.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomRepositoryService {

    @Autowired
    RoomRepository roomRepository;

    public Room save(Room room) {
        return roomRepository.save(room);
    }
    public List<Room> allRooms() {
        return roomRepository.findAll();
    }
    public Room findRoomById (int id){
        return roomRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }


}

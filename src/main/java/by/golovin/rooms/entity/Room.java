package by.golovin.rooms.entity;

import javax.persistence.*;

@Entity
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String name;

    @ManyToOne
    @JoinColumn(name = "country")
    private Country country;

    @Column
    private Boolean isEnabledLamp;

    public Room() {
        this.isEnabledLamp=false;
    }

    public Room(String name, Country country) {
        this.name = name;
        this.country = country;
        this.isEnabledLamp = false;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Boolean getEnabledLamp() {
        return isEnabledLamp;
    }

    public void setEnabledLamp(Boolean enabledLamp) {
        isEnabledLamp = enabledLamp;
    }
}

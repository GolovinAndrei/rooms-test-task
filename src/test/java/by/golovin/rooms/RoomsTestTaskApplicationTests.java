package by.golovin.rooms;

import by.golovin.rooms.service.AccessControl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.servlet.http.HttpServletRequest;

@SpringBootTest
class RoomsTestTaskApplicationTests {

	@Autowired
	HttpServletRequest request;
	@Test
	void contextLoads() {
		AccessControl ac = new AccessControl();

		//System.out.println(ac.findRemoteAddress(request));
		System.out.println(ac.findLocation("46.53.251.210"));
	}

}
